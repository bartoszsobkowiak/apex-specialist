public with sharing class WarehouseCalloutService {

    private static final String WAREHOUSE_URL = 'https://th-superbadge-apex.herokuapp.com/equipment';
    
    @future(callout=true)
    public static void runWarehouseEquipmentSync() {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(WAREHOUSE_URL);
        request.setMethod('GET');
        HttpResponse response = http.send(request);

        if (response.getStatusCode() == 200) {
            List<Object> results = (List<Object>) JSON.deserializeUntyped(response.getBody());
            List<Product2> productsToUpdate = new List<Product2>();
            for (Object obj : results){
                Product2 newProduct = new Product2();
                Map<String, Object> productMapped = (Map<String, Object>) obj;

                newProduct.ProductCode = (String) productMapped.get('_id');
                newProduct.Cost__c = (Integer) productMapped.get('cost');
                newProduct.Lifespan_Months__c = (Integer) productMapped.get('lifespan');
                newProduct.Maintenance_Cycle__c = (Integer) productMapped.get('maintenanceperiod');
                newProduct.Name = (String) productMapped.get('name');
                newProduct.Current_Inventory__c = (Integer) productMapped.get('quantity');
                newProduct.Replacement_Part__c = (Boolean) productMapped.get('replacement');
                newProduct.Warehouse_SKU__c = (String) productMapped.get('sku');

                productsToUpdate.add(newProduct);
            }
            if (productsToUpdate.size() > 0){
                upsert productsToUpdate;
            }
        }
                
    }
}