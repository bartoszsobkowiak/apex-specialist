public class MaintenanceRequestHelper {
    
    public static void updateWorkOrders(List<Case> updatedCases){
        List<Id> caseIdToProcess = new List<Id>(); 
        for (Case c: updatedCases) {

            if (c.Status == 'Closed') {
                if (c.Type == 'Repair' || c.Type == 'Routine Maintenance') {
                    caseIdToProcess.add(c.Id);
                }
            }
        } 
    
        if (caseIdToProcess.size() > 0) {
            List<Case> newCases = new List<Case>();
            Map<Id, Case> closedCaseMap = new Map<Id,Case>([SELECT Id, Vehicle__c, Equipment__c, Origin, Equipment__r.Maintenance_Cycle__c, (SELECT Id, Equipment__c, Quantity__c FROM Work_Parts__r) 
                                        FROM Case 
                                        WHERE Id IN :caseIdToProcess]); // map instead of list to get proper Work_Parts_r later

            
            AggregateResult[] results = [SELECT Maintenance_Request__c, MIN(Equipment__r.Maintenance_Cycle__c)cycle
           								 FROM Work_Part__c 
                                         WHERE Maintenance_Request__c IN :caseIdToProcess 
            							 GROUP BY Maintenance_Request__c]; // !
		

            Map<Id, Decimal> maintCycleMap = new Map<Id, Decimal>();
            for (AggregateResult ar : results) {
                maintCycleMap.put((Id) ar.get('Maintenance_Request__c'), (Decimal) ar.get('cycle') );
            }

            for (Case cs: closedCaseMap.values()){ // map
                Case newCase = new Case (
                                    ParentId = cs.Id, // !
                                    Status = 'New',
                                    Subject = 'Routine Maintenance',
                                    Type = 'Routine Maintenance',
                                    Vehicle__c = cs.Vehicle__c,
                                    Equipment__c = cs.Equipment__c,
                                    Origin = cs.Origin,    
                                    Date_Reported__c = Date.today());

                if (maintCycleMap.containsKey(cs.Id) ) {
                    newCase.Date_Due__c = Date.today().addDays(
                            (Integer) maintCycleMap.get(cs.Id)
                        );
                } else { // !
                    newCase.Date_Due__c = Date.today().addDays(
                            (Integer) cs.Equipment__r.Maintenance_Cycle__c
                        );
                } 
                
                newCases.add(newCase);
            }
            insert newCases;
            

            List<Work_Part__c> workparts = new List<Work_Part__c>();
            for (Case cs: newCases) {

                List<Work_Part__c> caseWorkParts = closedCaseMap.get(cs.ParentId).Work_Parts__r; // !

                for (Work_Part__c wp: caseWorkParts) {
                    Work_Part__c newWorkPart = wp.clone(); // make mess if not cloned
                    newWorkPart.Maintenance_Request__c = cs.Id;
                    workparts.add(newWorkPart);
                }
            }
            insert workparts;

        }
    }        
}