@IsTest
public class InstallationTests {
    @isTest 
    public static void testUpdateWorkOrders(){
        Integer maintCycle = 10;
        Vehicle__c vehicle = new Vehicle__c(Name='Test Vehicle');
        Product2 equipment = new Product2(Name='Test Product2', Maintenance_Cycle__c=maintCycle, Lifespan_Months__c=1, Replacement_Part__c =true);

        insert vehicle;
        insert equipment;

        Case newRoutineMaintenance = new Case(
            Vehicle__c = vehicle.Id,
            Equipment__c = equipment.Id,
            Type = 'Routine Maintenance',
            Subject = 'Routine Maintenance',
            Status = 'New',
            Date_Reported__c = Date.today()
        );
        insert newRoutineMaintenance;

        Work_Part__c wpc = new Work_Part__c(Equipment__c = equipment.Id, Maintenance_Request__c = newRoutineMaintenance.Id);
        insert wpc;


        newRoutineMaintenance.Status = 'Closed';
        update newRoutineMaintenance;

        //MaintenanceRequestHelper.updateWorkOrders();

        List<Case> requests = [SELECT Id, Vehicle__c, Equipment__c, Type, Subject, Status, Date_Due__c
                                FROM Case
                                WHERE Status='New' AND Type = 'Routine Maintenance' AND Subject = 'Routine Maintenance'
                                AND Vehicle__c =: vehicle.Id AND Equipment__c =: equipment.Id];

        System.assert(requests.size() > 0);

        Boolean confirmed = False;
        for (Case req : requests){
            if (req.Date_Due__c.daysBetween(Date.today().addDays(maintCycle)) < 1){
                confirmed = True;
            }
        }
        System.assert(confirmed);

    }


    @isTest 
    public static void testUpdateWorkOrdersBulk(){
        Integer maintCycle = 10;
        List<Case> cases = TestDataFactory.generateCases(300, maintCycle);
        for (Case cs : cases){
            cs.Status = 'Closed';
        }      
        update cases;

        Date lessDate = Date.today().addDays(maintCycle+1);
        Date moreDate = Date.today().addDays(maintCycle-1);
        List<Case> requests = [SELECT Id, Vehicle__c, Equipment__c, Type, Subject, Status, Date_Due__c
                                FROM Case
                                WHERE Status='New' AND Type = 'Routine Maintenance' AND Subject = 'Routine Maintenance'
                                AND Date_Due__c < :lessDate AND Date_Due__c > :moreDate];

        System.assert(requests.size() > 299);

        Boolean confirmed = False;
        for (Case req : requests){
            if (req.Date_Due__c.daysBetween(Date.today().addDays(maintCycle)) < 1){
                confirmed = True;
            }
        }
        System.assert(confirmed);

    }


    @isTest 
    public static void testUpdateWorkOrdersWithNoWorkParts(){
        Integer maintCycle = 10;
        Vehicle__c vehicle = new Vehicle__c(Name='Test Vehicle');
        Product2 equipment = new Product2(Name='Test Product2', Maintenance_Cycle__c=maintCycle, Lifespan_Months__c=1, Replacement_Part__c =true);

        insert vehicle;
        insert equipment;

        Case newRoutineMaintenance = new Case(
            Vehicle__c = vehicle.Id,
            Equipment__c = equipment.Id,
            Type = 'Routine Maintenance',
            Subject = 'Routine Maintenance',
            Status = 'New',
            Date_Reported__c = Date.today()
        );
        insert newRoutineMaintenance;

        // Work_Part__c wpc = new Work_Part__c(Equipment__c = equipment.Id, Maintenance_Request__c = newRoutineMaintenance.Id);
        // insert wpc;


        newRoutineMaintenance.Status = 'Closed';
        update newRoutineMaintenance;

        List<Case> requests = [SELECT Id, Vehicle__c, Equipment__c, Type, Subject, Status, Date_Due__c
                                FROM Case
                                WHERE Status='New' AND Type = 'Routine Maintenance' AND Subject = 'Routine Maintenance'
                                AND Vehicle__c =: vehicle.Id AND Equipment__c =: equipment.Id];

        System.assert(requests.size() > 0);

        Boolean confirmed = False;
        for (Case req : requests){
            if (req.Date_Due__c.daysBetween(Date.today().addDays(maintCycle)) < 1){
                confirmed = True;
            }
        }
        System.assert(confirmed);

    }

    static testmethod void testWarehouseSyncSchedule(){
        String dateAndTime = '0 0 1 * * ?';
        Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock()); 

        Test.startTest();
        String jobId = System.schedule('ScheduledWarehouseSyncScheduleTest',
            dateAndTime, 
            new WarehouseSyncSchedule());         
        Test.stopTest();

        List<CronTrigger> jobs = [SELECT Id, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType 
                            FROM CronTrigger 
                            WHERE CronJobDetail.Name = 'ScheduledWarehouseSyncScheduleTest'];

        System.assert(jobs.size() > 0);

    }


}