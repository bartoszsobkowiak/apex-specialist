public with sharing class TestDataFactory {
    public static List<Case> generateCases(Integer amount, Integer maintCycle){
        List<Case> cases = new List<Case>();
        List<Work_Part__c> workparts = new List<Work_Part__c>();
        List<Vehicle__c> vehicles = new List<Vehicle__c>();
        List<Product2> equipments = new List<Product2>();
        

        for (Integer i=0; i<amount; i++){
            vehicles.add(
                new Vehicle__c(Name='Test Vehicle')
                );
            equipments.add(
                new Product2(Name='Test Product2', Maintenance_Cycle__c=maintCycle, Lifespan_Months__c=1, Replacement_Part__c =true)
            );
        }
        insert vehicles;
        insert equipments;

        for (Integer i=0; i<amount; i++){
            Case newRoutineMaintenance = new Case(
                Vehicle__c = vehicles[i].Id,
                Equipment__c = equipments[i].Id,
                Type = 'Routine Maintenance',
                Subject = 'Routine Maintenance',
                Status = 'New',
                Date_Reported__c = Date.today()
            );
            cases.add(newRoutineMaintenance);
        }

        insert cases;

        for (Integer i=0; i<amount; i++){
            Work_Part__c wpc = new Work_Part__c(Equipment__c = equipments[i].Id, Maintenance_Request__c = cases[i].Id);
            workparts.add(wpc);
        }
        insert workparts;

        return cases;

    }
}
