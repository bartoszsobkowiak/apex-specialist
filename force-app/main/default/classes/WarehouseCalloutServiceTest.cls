@IsTest
private class WarehouseCalloutServiceTest {
    @isTest static void testCallout(){
        Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock()); 
        WarehouseCalloutService.runWarehouseEquipmentSync();

        List<Product2> updatedProducts = [SELECT ProductCode, Name, Cost__c, Lifespan_Months__c, Maintenance_Cycle__c, Current_Inventory__c, Replacement_Part__c, Warehouse_SKU__c 
                                FROM Product2
                                WHERE ProductCode = '55d66226726b611100aaf741'];

        for (Product2 updatedProduct : updatedProducts){
            System.assertEquals('Generator 1000 kW', updatedProduct.Name);        
            System.assertEquals(5000, updatedProduct.Cost__c);   
            System.assertEquals('100003', updatedProduct.Warehouse_SKU__c);      
        }
        
    }

}